﻿using ChessLogic.Pieces;
using System;
using UnityEngine;

public abstract class UnityChessPiece : MonoBehaviour, ICloneable
{
    public abstract object Clone();

    IChessPiece Piece;
    public int Row { get; set; }
    //static ChessPieceBase chessPieceBase;
    //ChessBoardReadOnly board;

    //public bool isWhite = chessPieceBase.Color == ChessColor.White ? true : false;
    //public int CurrentRow = chessPieceBase.Row;
    //public int CurrentColumn = chessPieceBase.Column;

    //public virtual bool[,] PossibleMove()
    //{
    //    var result = new bool[8, 8];

    //    if (!board.IsPiece(CurrentRow, CurrentColumn))
    //        result[CurrentRow, CurrentColumn] = true;
    //    return result;
    //}
    //public void SetPosition(int x, int y)
    //{
    //    CurrentRow = x;
    //    CurrentColumn = y;
    //}

    //public (int, int) Coordinates
    //{
    //    get => (CurrentRow, CurrentColumn);
    //    set
    //    {
    //        CurrentRow = value.Item1;
    //        CurrentColumn = value.Item2;
    //    }
    //}
}