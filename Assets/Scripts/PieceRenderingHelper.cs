﻿using ChessLogic;
using ChessLogic.Pieces;
using System.Collections.Generic;
using UnityEngine;

public class PieceRenderingHelper : MonoBehaviour
{

    private const float TILE_SIZE = 1.0f;
    private const float TILE_OFFSET = 0.5f;

    public static PieceRenderingHelper Instance { get; set; }

    private void Start()
    {
        Instance = this;
    }

    public void DrawChessBoard(int selectionX, int selectionY)
    {
        Vector3 widthLine = Vector3.right * 8;
        Vector3 heightLine = Vector3.forward * 8;

        for (int i = 0; i <= 8; i++)
        {
            Vector3 start = Vector3.forward * i;

            Debug.DrawLine(start, start + widthLine);
            for (int j = 0; j <= 8; j++)
            {
                start = Vector3.right * j;
                Debug.DrawLine(start, start + heightLine);

            }
        }

        //Draw the selction 
        if (selectionX >= 0 && selectionY >= 0)
        {
            Debug.DrawLine(Vector3.forward * selectionY + Vector3.right * selectionX,
                            Vector3.forward * (selectionY + 1) + Vector3.right * (selectionX + 1));
            Debug.DrawLine(Vector3.forward * (selectionY + 1) + Vector3.right * selectionX,
                            Vector3.forward * selectionY + Vector3.right * (selectionX + 1));
        }

    }

    public UnityChessPiece[,] SpawnAllPieces(ChessGame game, List<GameObject> chessPiecesPrefabs, List<GameObject> activeChessman)
    {
        var chessPieces = new UnityChessPiece[8, 8];

        //Pawn
        for (var i = 0; i < 8; i++)
            for (var j = 0; j < 8; j++)
                SpawnChessPiece(chessPieces, chessPiecesPrefabs, activeChessman, game.Board[i, j]);

        return chessPieces;
    }

    private void SpawnChessPiece(UnityChessPiece[,] chessPieces, List<GameObject> chessPiecesPrefabs, 
        List<GameObject> activeChessman, IChessPiece piece)
    {
        var index = GetPiecePrefabIndex(piece);
        if (index == -1)
            return;

        var (x, y) = piece.Coordinates;
        GameObject go = Instantiate(chessPiecesPrefabs[index], GetTileCenter(x, y), Quaternion.identity) as GameObject;
        go.transform.SetParent(transform);

        chessPieces[x, y] = go.GetComponent<UnityChessPiece>();
        chessPieces[x, y].SetPosition(x, y);

        if (chessPieces[x, y].isWhite)
            go.transform.Rotate(0f, -90f, 0f);
        else
            go.transform.Rotate(0f, 90f, 0f);

        activeChessman.Add(go);
    }

    public Vector3 GetTileCenter(int x, int y)
    {
        Vector3 origin = Vector3.zero;
        origin.x += (TILE_SIZE * x) + TILE_OFFSET;
        origin.z += (TILE_SIZE * y) + TILE_OFFSET;
        return origin;
    }

    private int GetPiecePrefabIndex(IChessPiece piece)
    {
        var index = -1;
        if (piece is ChessLogic.Pieces.Rook)
            index = piece.Color == ChessColor.Black ? 0 : 6;

        if (piece is ChessLogic.Pieces.Knight)
            index = piece.Color == ChessColor.Black ? 1 : 7;

        if (piece is ChessLogic.Pieces.Bishop)
            index = piece.Color == ChessColor.Black ? 2 : 8;

        if (piece is ChessLogic.Pieces.Queen)
            index = piece.Color == ChessColor.Black ? 3 : 9;

        if (piece is ChessLogic.Pieces.King)
            index = piece.Color == ChessColor.Black ? 4 : 10;

        if (piece is ChessLogic.Pieces.Pawn)
            index = piece.Color == ChessColor.Black ? 5 : 11;

        return index;
    }
}
