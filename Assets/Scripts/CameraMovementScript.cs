﻿using UnityEngine;

public class CameraMovementScript : MonoBehaviour
{
    public GameObject CameraFocus = null;

    public float lookSpeedH = 2f;
    public float lookSpeedV = 2f;
    public float zoomSpeed = 2f;
    public float dragSpeed = 1f;

    void Update()
    {
        if (Input.GetMouseButton(2))
        {
            transform.Translate(-Input.GetAxisRaw("Mouse X") * Time.deltaTime * dragSpeed, 0, 0);
            transform.LookAt(CameraFocus.transform);
        }


        //Zoom in and out with Mouse Wheel
        if ((int)transform.position.y > 15)
        {
            if ((int)transform.position.y != 17)
                transform.Translate(0, 0, Input.GetAxis("Mouse ScrollWheel") * zoomSpeed, Space.Self);
            else if (Input.GetAxis("Mouse ScrollWheel") > 0)
                transform.Translate(0, 0, Input.GetAxis("Mouse ScrollWheel") * zoomSpeed, Space.Self);
        }
        else
        {
            if ((int)transform.position.y != 10)
                transform.Translate(0, 0, Input.GetAxis("Mouse ScrollWheel") * zoomSpeed, Space.Self);
            else if (Input.GetAxis("Mouse ScrollWheel") < 0)
                transform.Translate(0, 0, Input.GetAxis("Mouse ScrollWheel") * zoomSpeed, Space.Self);
        }


    }
}
