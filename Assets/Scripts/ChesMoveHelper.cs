﻿using System.Collections.Generic;
using UnityEngine;

class ChesMoveHelper : MonoBehaviour
{
    public static ChesMoveHelper Instance { get; set; }

    private void Start()
    {
        Instance = this;
    }

    public UnityChessPiece SelectChessPiece(UnityChessPiece[,] chessPieces,bool isWhiteTurn, int x, int y)
    {
        if (chessPieces[x, y] == null || chessPieces[x, y].isWhite != isWhiteTurn)
            return null;           

        bool hasAtleastOneMove = false;
        var allowedMoves = chessPieces[x, y].PossibleMove();
        for (int i = 0; i < 8; i++)
            for (int j = 0; j < 8; j++)
                if (allowedMoves[i, j])
                    hasAtleastOneMove = true;

        if (!hasAtleastOneMove)
            return null;

        chessPieces[x, y].transform.Translate(Vector3.up * 0.5f, Space.Self);
        BoardHighlights.Instance.HighlightAllowedMoves(allowedMoves);
        return chessPieces[x, y];
    }

    public (bool isWhiteTurn, UnityChessPiece selectedPiece) MoveChessPiece(UnityChessPiece[,] ChessPieces, bool isWhiteTurn, 
        List<GameObject> activeChessman, UnityChessPiece selectedPiece, int x, int y)
    {
        if(selectedPiece == null)
        return (isWhiteTurn, selectedPiece);

        var allowedMoves = selectedPiece.PossibleMove();
        if (allowedMoves[x, y])
        {
            UnityChessPiece otherPiece = ChessPieces[x, y];

            if (otherPiece != null && otherPiece.isWhite != isWhiteTurn)
            {
                //If the king end
                if (otherPiece.GetType() == typeof(ChessLogic.Pieces.King))
                {
                    //EndGame();
                    return (isWhiteTurn, selectedPiece);
                }

                activeChessman.Remove(otherPiece.gameObject);
                Destroy(otherPiece.gameObject);
            }

            //Pawn promotion
            //if (selectedPiece.GetType() == typeof(Pawn))
            //{
            //    if (x == 0)
            //    {
            //        activeChessman.Remove(selectedPiece.gameObject);
            //        Destroy(selectedPiece.gameObject);
            //        PieceRenderingHelper.Instance.SpawnChessPiece(ChessPieces, ChessPiecesPrefabs, activeChessman, 9, x, y);
            //        selectedPiece = ChessPieces[x, y];
            //    }
            //    else if (x == 7)
            //    {
            //        activeChessman.Remove(selectedPiece.gameObject);
            //        Destroy(selectedPiece.gameObject);
            //        PieceRenderingHelper.Instance.SpawnChessPiece(ChessPieces, ChessPiecesPrefabs, activeChessman, 3, x, y);
            //        selectedPiece = ChessPieces[x, y];
            //    }
            //}

            ChessPieces[selectedPiece.CurrentRow, selectedPiece.CurrentColumn] = null;


            selectedPiece.transform.position = PieceRenderingHelper.Instance.GetTileCenter(x, y);
            selectedPiece.SetPosition(x, y);
            ChessPieces[x, y] = selectedPiece;
            isWhiteTurn = !isWhiteTurn;
        }
        else
        {
            selectedPiece.transform.Translate(Vector3.down * 0.5f, Space.Self);
        }

        BoardHighlights.Instance.HideHighlights();
        selectedPiece = null;

        return (isWhiteTurn, selectedPiece);
    }
}
