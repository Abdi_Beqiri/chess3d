﻿using ChessLogic;
using System.Collections.Generic;
using UnityEngine;

public class BoardManager : MonoBehaviour
{
    public static BoardManager Instance { get; set; }

    private ChessGame _game;
    private ChessPieceSelector _pieceSelector;

    public UnityChessPiece[,] ChessPieces { get; set; }
    private UnityChessPiece selectedPiece;


    private int selectionX = -1;
    private int selectionY = -1;

    public List<GameObject> ChessPiecesPrefabs;
    private List<GameObject> activeChessman = new List<GameObject>();

    public bool isWhiteTurn = true;
    private void Start()
    {
        Instance = this;
        _game = new ChessGame();
        _pieceSelector = new ChessPieceSelector();
        //ChessPieces = PieceRenderingHelper.Instance.SpawnAllPieces(_game, ChessPiecesPrefabs, activeChessman);
    }
    private void Update()
    {
        UpdateSelection();
        if (ChessPieces == null && PieceRenderingHelper.Instance == null)
            return;
        else if(ChessPieces == null && PieceRenderingHelper.Instance != null)
            ChessPieces = PieceRenderingHelper.Instance.SpawnAllPieces(_game, ChessPiecesPrefabs, activeChessman);

        PieceRenderingHelper.Instance.DrawChessBoard(selectionX, selectionY);

        if (Input.GetMouseButtonDown(0))
        {
            if (selectionX >= 0 && selectionY >= 0)
            {
                if (selectedPiece == null)
                {
                    //select piece
                    selectedPiece = ChesMoveHelper.Instance.SelectChessPiece(ChessPieces,isWhiteTurn,selectionX, selectionY);
                }
                else
                {
                    //move piece
                    (isWhiteTurn,  selectedPiece) = ChesMoveHelper.Instance.MoveChessPiece(ChessPieces, isWhiteTurn, activeChessman,
                         selectedPiece, selectionX, selectionY);
                }
            }
        }
    }

    
    private void UpdateSelection()
    {
        if (!Camera.main)
            return;

        RaycastHit hit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 25.0f, LayerMask.GetMask("ChessPlane")))
        {
            selectionX = (int)hit.point.x;
            selectionY = (int)hit.point.z;
        }
        else
        {
            selectionX = -1;
            selectionY = -1;
        }
    }

    private void EndGame()
    {
        if (isWhiteTurn)
            Debug.Log("White team wins");
        else
            Debug.Log("Black team wins");

        foreach (GameObject go in activeChessman)
            Destroy(go);

        isWhiteTurn = true;
        BoardHighlights.Instance.HideHighlights();
        ChessPieces = PieceRenderingHelper.Instance.SpawnAllPieces(_game,ChessPiecesPrefabs, activeChessman);
    }
}